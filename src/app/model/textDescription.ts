import { Deserializable } from './deserializable';

export class TextDescription implements Deserializable {
    id: number;
    subHeading: string;
    heading: string;
    description: string;

    deserialize(input: any): this {
        Object.assign(this, input);
        return this;
    }
}
