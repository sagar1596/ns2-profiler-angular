import { TextDescription } from '../textDescription';
import { Deserializable } from '../deserializable';

export class Slider implements Deserializable {
  id: number;
  imagePath: string;
  description: TextDescription;

  deserialize(input: any) {
    Object.assign(this, input);
    this.description = new TextDescription().deserialize(input.description);
    return this;
  }
}
