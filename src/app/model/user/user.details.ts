import { User } from './user';

export class UserDetails {
  id: number;
  user: User;
  displayname: string;
  firstName: string;
  lastName: string;
  token: string;
}
