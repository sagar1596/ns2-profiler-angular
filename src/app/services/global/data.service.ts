import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private userStatusSource = new BehaviorSubject(false);
  userStatusCurrent = this.userStatusSource.asObservable();

  constructor() { }

  changeUserStatus(userStatus: boolean) {
    this.userStatusSource.next(userStatus);
  }
}
