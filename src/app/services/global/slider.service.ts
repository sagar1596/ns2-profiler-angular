import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Slider } from '../../model/global/slider';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  constructor(private http: HttpClient) {}
}
