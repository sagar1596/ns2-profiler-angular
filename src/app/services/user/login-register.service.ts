import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../../model/user/user';
import { UserDetails } from '../../model/user/user.details';

@Injectable({
  providedIn: 'root'
})
export class LoginRegisterService {

  private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient, private user: User, private userDetails: UserDetails) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {

        return this.http.post('http://localhost:4000/users/authenticate',
        { username: username, password: password })
        .pipe(map(user => {
            if (user && user['token']) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('user', JSON.stringify(user));
            }
            return user;
         }));
    }

    register(userObject: UserDetails) {
        this.http.post('http://localhost:4000/users/register', userObject).subscribe(obj => {
            console.log('call gone');
        });
        // .pipe(map(user => {
        //     console.log(user);
        //     return user;
        //  }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    loginLogoutClick() {
        if (document.querySelectorAll('body')[0].classList.contains('login-reg-show')) {
            document.querySelectorAll('body')[0].classList.remove('login-reg-show');
            // tslint:disable-next-line:max-line-length
            document.querySelectorAll('#fh5co-main-nav-login-register > .js-fh5co-nav-toggle-login-register')[0].classList.remove('show');
        } else {
            document.querySelectorAll('body')[0].classList.add('login-reg-show');
            setTimeout( () => {
                // tslint:disable-next-line:max-line-length
                document.querySelectorAll('#fh5co-main-nav-login-register > .js-fh5co-nav-toggle-login-register')[0].classList.add('show');
            }, 900);
        }
      }
}
