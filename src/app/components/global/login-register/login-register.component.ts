import { Component, OnInit } from '@angular/core';
import { LoginRegisterService } from '../../../services/user/login-register.service';

import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { DataService } from '../../../services/global/data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { User } from '../../../model/user/user';
import { UserDetails } from '../../../model/user/user.details';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.scss']
})
export class LoginRegisterComponent implements OnInit {

  userStatus: boolean;

  constructor(private userService: LoginRegisterService,
    private httppClient: HttpClient,
    private router: Router,
    private data: DataService,
    private userData: User,
    private userDetails: UserDetails) { }

  ngOnInit() {
    this.data.userStatusCurrent.subscribe(userStatus => this.userStatus = userStatus);
  }

  loginClick() {
    const loginFields = document.querySelectorAll('[data-type="login"]');
    this.userService.login(this.userData.username, this.userData.password)
    .subscribe(
      () => {
          document.querySelectorAll('body')[0].classList.remove('login-reg-show');
          document.querySelectorAll('#fh5co-main-nav-login-register > .js-fh5co-nav-toggle-login-register')[0].classList.remove('show');

          this.data.changeUserStatus(true);
          this.data.userStatusCurrent.subscribe(userStatus => this.userStatus = userStatus);
      },
      () => {
          console.log('Error while logging in');
      });
  }

  loginLogoutClick() {
    this.userService.loginLogoutClick();
  }

  registerClick() {
    this.userService.register(this.userDetails);
  }

}
