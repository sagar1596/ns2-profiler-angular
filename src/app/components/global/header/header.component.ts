import { Component, OnInit } from '@angular/core';

import { DataService } from '../../../services/global/data.service';
import { LoginRegisterService } from '../../../services/user/login-register.service';

@Component({
  selector: 'app-header-global',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderGlobalComponent implements OnInit {
  userStatus: boolean;
  constructor(private data: DataService,
    private loginRegisterService: LoginRegisterService) {
    this.userStatus = false;
  }

  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('user'));
    if (currentUser && currentUser.token) {
      this.data.changeUserStatus(true);
      this.userStatusSubscribe();
    } else {
      this.data.changeUserStatus(false);
      this.userStatusSubscribe();
    }
  }

  userStatusSubscribe() {
    this.data.userStatusCurrent.subscribe(userStatus => this.userStatus = userStatus);
  }

  loginLogoutClick() {
    // tslint:disable-next-line:max-line-length
    if (document.querySelectorAll('.js-fh5co-nav-toggle-login.js-fh5co-nav-toggle-login-register')[0].getAttribute('data-login') === 'guest') {
      this.loginRegisterService.loginLogoutClick();
    } else {
      localStorage.removeItem('user');
      this.data.changeUserStatus(false);
      this.userStatusSubscribe();
    }
  }
}

