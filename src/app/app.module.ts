import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { JwtInterceptor } from './_helpers/jwt.interceptor';

import { SliderService } from '../app/services/global/slider.service';
import { LoginRegisterService } from '../app/services/user/login-register.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './layouts/home/home.component';
import { NavigationComponent } from './components/global/navigation/navigation.component';
import { HeaderGlobalComponent } from './components/global/header/header.component';
import { SliderComponent } from './components/global/slider/slider.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PanelComponent } from './components/global/panel/panel.component';
import { CounterComponent } from './components/global/counter/counter.component';
import { FooterGlobalComponent } from './components/global/footer/footer.component';
import { LoginRegisterComponent } from './components/global/login-register/login-register.component';

import { User } from './model/user/user';
import { UserDetails } from './model/user/user.details';
import { StichComponent } from './layouts/stich/stich.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    NavigationComponent,
    HeaderGlobalComponent,
    SliderComponent,
    PanelComponent,
    CounterComponent,
    FooterGlobalComponent,
    LoginRegisterComponent,
    StichComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SliderService,
    LoginRegisterService,
    User,
    UserDetails,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
